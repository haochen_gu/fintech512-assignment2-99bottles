package assignment2;

class Bottles {

	public String verse(int verseNumber) {

		String expected = "";
		String number = String.valueOf(verseNumber);
		String minusOne = String.valueOf(verseNumber-1);

		if (verseNumber > 1) {
			expected = number + " bottles of beer on the wall, " + number +  " bottles of beer.\n"
					+ "Take one down and pass it around, " + minusOne +  " bottles of beer on the wall.\n";
			if (verseNumber == 2) {
				expected = number + " bottles of beer on the wall, " + number +  " bottles of beer.\n"
						+ "Take one down and pass it around, " + minusOne +  " bottle of beer on the wall.\n";
			}
		}

		else if (verseNumber == 1) {
			expected = "1 bottle of beer on the wall, " + "1 bottle of beer.\n" +
					"Take it down and pass it around, " + "no more bottles of beer on the wall.\n";
		}

		else {
			expected = "No more bottles of beer on the wall, " + "no more bottles of beer.\n"
					+ "Go to the store and buy some more, " + "99 bottles of beer on the wall.\n";;
		}

		return expected;
	}

	public String verse(int startVerseNumber, int endVerseNumber) {

		String s= "";

		for (int i=startVerseNumber; i>=endVerseNumber; i--) {
			s = s + verse(i);
			if (i>endVerseNumber) {
				s = s + "\n";
			}
		}

		return s;

	}

	public String song() {
		String s = "";
		for (int i=99; i>=0; i--) {
			s = s + verse(i);
			if (i>0) {
				s = s + "\n";
			}
		}
		return s;
	}
}
